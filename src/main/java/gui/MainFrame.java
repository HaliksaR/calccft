package gui;

import core.CoreCalculator;
import core.MyMenuListener;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import static java.awt.Toolkit.getDefaultToolkit;

public class MainFrame extends JFrame implements ActionListener {

    private JLabel JLOutput;
    private JTextField JFTextLine;
    private JButton JBEnter, JBClear;

    public MainFrame() {
        super("Calculator");
        setDefaultCloseOperation(EXIT_ON_CLOSE);

        JMenu JMAbout = new JMenu("About");
        JMAbout.addMenuListener(new MyMenuListener(this));
        JMenuBar JMBar = new JMenuBar();
        JMBar.add(JMAbout);
        setJMenuBar(JMBar);

        Box boxTextLine = Box.createHorizontalBox();
        JFTextLine = new JTextField(20);
        JFTextLine.setHorizontalAlignment(JTextField.CENTER);
        boxTextLine.add(JFTextLine);

        Box boxLabel = Box.createHorizontalBox();
        JLOutput = new JLabel("Here will be the answer");
        JLOutput.setOpaque(true);
        boxLabel.add(JLOutput);

        Box boxButtons = Box.createHorizontalBox();
        JBEnter = new JButton("Ok");
        JBEnter.addActionListener(this);
        boxButtons.add(JBEnter);
        JBClear = new JButton("Clear");
        JBClear.addActionListener(this);
        boxButtons.add(JBClear);

        Box boxMain = Box.createVerticalBox();
        boxMain.setBorder(new EmptyBorder(20, 10, 20, 10));
        boxMain.add(boxTextLine);
        boxMain.add(Box.createVerticalStrut(20));
        boxMain.add(boxLabel);
        boxMain.add(Box.createVerticalStrut(20));
        boxMain.add(boxButtons);

        setContentPane(boxMain);
        pack();

        setLocation(
                (getDefaultToolkit().getScreenSize().width - getWidth()) / 2,
                (getDefaultToolkit().getScreenSize().height - getHeight()) / 2
        );
        setVisible(true);
        setResizable(false);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (!JFTextLine.getText().equals("")) {
            Object source = e.getSource();
            if (JBEnter.equals(source)) {
                CoreCalculator CoreCalc = new CoreCalculator();
                if (CoreCalc.calculate(JFTextLine.getText())) JLOutput.setText(CoreCalc.getAnswer().toString());
                else JLOutput.setText(CoreCalc.getException());
            } else if (JBClear.equals(source)) {
                JLOutput.setText("Here will be the answer");
                JFTextLine.setText("");
            } else throw new IllegalStateException("Unexpected value: " + e.getSource());
        }
    }
}
