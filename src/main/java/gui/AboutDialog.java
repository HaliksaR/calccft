package gui;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import static java.awt.Toolkit.getDefaultToolkit;

public class AboutDialog extends JDialog {
    public AboutDialog(JFrame parent, String title, boolean modal) {
        super(parent, title, modal);
        JPanel JPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
        JLabel JLabel = new JLabel("Developer: HaliksaR");
        JLabel.setHorizontalAlignment(JTextField.CENTER);
        JLabel.setBorder(new EmptyBorder(10, 10, 10, 10));
        JPanel.add(JLabel);
        setContentPane(JPanel);
        pack();
        setLocation(
                (getDefaultToolkit().getScreenSize().width - getWidth()) / 2,
                (getDefaultToolkit().getScreenSize().height - getHeight()) / 2
        );
        setVisible(true);
        setResizable(false);
        addWindowListener(new WindowAdapter() {
                              public void windowClosing(WindowEvent e) {
                                  Window aboutDialog = e.getWindow();
                                  aboutDialog.dispose();
                              }
                          }
        );
    }
}
