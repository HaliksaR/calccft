package core;

import java.util.Stack;

public class CoreCalculator {
    private Double Answer;

    public Double getAnswer() {
        return Answer;
    }

    private String exception;

    public String getException() {
        return exception;
    }

    private final String NUMBERS = "1234567890";
    private final String OPS = "+-*/()";
    private final String DOTS = ",.";

    public boolean calculate(String expression) {
        expression = clearSpaces(expression);
        expression = correctPoints(expression);
        Stack<Double> values = new Stack<>();
        Stack<Character> ops = new Stack<>();
        Double tmp;
        boolean flagUnary = false;
        int brackets = 0;
        for (int i = 0; i < expression.length(); i++) {
            if (isDigit(expression.charAt(i))) {
                StringBuilder StrBuf = new StringBuilder();
                while (i < expression.length() && (isDigit(expression.charAt(i)) || isDot(expression.charAt(i)))) {
                    StrBuf.append(expression.charAt(i++));
                }
                i--;
                byte numDot = 0;
                for (int j = 0; j < StrBuf.length(); j++)
                    if (isDot(StrBuf.charAt(j)))
                        numDot++;
                if (numDot < 2)
                    if (flagUnary)
                        values.add(0 - Double.parseDouble(StrBuf.toString()));
                    else
                        values.add(Double.parseDouble(StrBuf.toString()));
                else {
                    exception = "Error Format";
                    return false;
                }
                flagUnary = false;
            } else if (isOperator(expression.charAt(i))) {
                if (i - 1 >= 0) {
                    if (expression.charAt(i) == '-' && expression.charAt(i - 1) == '(')
                        flagUnary = true;
                } else if (i == 0 && expression.charAt(i) == '-') {
                    flagUnary = true;
                }
                if (expression.charAt(i) == '(') {
                    ops.push(expression.charAt(i));
                    brackets++;
                } else if (expression.charAt(i) == ')') {
                    brackets--;
                    if (brackets < 0) {
                        exception = "Brackets Error";
                        return false;
                    }
                    while (ops.peek() != '(')
                        values.push(applyOp(ops.pop(), values.pop(), values.pop()));
                    ops.pop();
                } else {
                    while (!ops.empty() && hasPrecedence(expression.charAt(i), ops.peek())) {
                        tmp = applyOp(ops.pop(), values.pop(), values.pop());
                        if (tmp == null) {
                            exception = "Calc Error";
                            return false;
                        } else
                            values.push(tmp);
                    }
                    if (!flagUnary)
                        ops.push(expression.charAt(i));
                }
            } else {
                exception = "Error Format";
                return false;
            }
        }
        while (!ops.empty()) {
            tmp = applyOp(ops.pop(), values.pop(), values.pop());
            if (tmp == null) {
                exception = "Calc Error";
                return false;
            } else
                values.push(tmp);
        }
        Answer = values.pop();
        return true;
    }

    private boolean hasPrecedence(char op1, char op2) {
        if (op2 == '(' || op2 == ')')
            return false;
        if ((op1 == '*' || op1 == '/') && (op2 == '+' || op2 == '-'))
            return false;
        else
            return true;
    }

    private Double applyOp(char op, Double b, Double a) {
        switch (op) {
            case '+':
                return a + b;
            case '-':
                return a - b;
            case '*':
                return a * b;
            case '/':
                if (b == 0)
                    return null;
                return a / b;
            default:
                return null;
        }
    }

    private String clearSpaces(String s) {
        return s.replaceAll("\\s+", "");
    }

    private String correctPoints(String s) {
        return s.replaceAll(",", ".");
    }

    private boolean isDot(Character s) {
        return DOTS.contains(s.toString());
    }

    private boolean isOperator(Character s) {
        return OPS.contains(s.toString());
    }

    private boolean isDigit(Character s) {
        return NUMBERS.contains(s.toString());
    }
}
