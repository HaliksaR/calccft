package core;

import gui.AboutDialog;

import javax.swing.*;
import javax.swing.event.MenuEvent;

public class MyMenuListener implements javax.swing.event.MenuListener {
    private JFrame Parent;

    public MyMenuListener(JFrame parent) {
        Parent = parent;
    }

    @Override
    public void menuSelected(MenuEvent e) {
        new AboutDialog(Parent, "About Java Calculator", true);
    }

    @Override
    public void menuDeselected(MenuEvent e) {

    }

    @Override
    public void menuCanceled(MenuEvent e) {

    }
}
