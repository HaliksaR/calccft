import core.CoreCalculator;
import org.junit.Assert;
import org.junit.Test;

public class JTestCore {
    @Test
    public void core_return() {
        Assert.assertTrue(new CoreCalculator().calculate("2 + (-2 + 4 * 4.55) / 3"));
    }

    @Test
    public void core_return_answer() {
        CoreCalculator CCalc = new CoreCalculator();
        CCalc.calculate("-34.2*(4.2/4.2+(36-3*6/(-8+9)*(-7.6))+6)*2");
        String tmp = String.format("%.2f", CCalc.getAnswer());
        Assert.assertEquals(-12298.32, Double.parseDouble(tmp), 0.0);
    }

    @Test
    public void one_value() {
        CoreCalculator CCalc = new CoreCalculator();
        CCalc.calculate("4.55");
        Assert.assertEquals(4.55, CCalc.getAnswer(), 0.0);
    }

    @Test
    public void unary_operator() {
        CoreCalculator CCalc = new CoreCalculator();
        CCalc.calculate("-2.2");
        String tmp = String.format("%.1f", CCalc.getAnswer());
        Assert.assertEquals(-2.2, Double.parseDouble(tmp), 0.0);
    }

    @Test
    public void brackets_operator() {
        CoreCalculator CCalc = new CoreCalculator();
        CCalc.calculate("(-2.2/2)-1");
        String tmp = String.format("%.1f", CCalc.getAnswer());
        Assert.assertEquals(-2.1, Double.parseDouble(tmp), 0.0);
    }

    @Test
    public void invalid_brackets_operator() {
        Assert.assertFalse(new CoreCalculator().calculate("(-2.2/2))-1"));
    }

    @Test
    public void spacer() {
        CoreCalculator CCalc = new CoreCalculator();
        CCalc.calculate("(-2  .2    /2)-    1");
        String tmp = String.format("%.1f", CCalc.getAnswer());
        Assert.assertEquals(-2.1, Double.parseDouble(tmp), 0.0);
    }

    @Test
    public void invalid_dot() {
        Assert.assertFalse(new CoreCalculator().calculate("....3"));
    }

    @Test
    public void valid_dot() {
        Assert.assertTrue(new CoreCalculator().calculate("0.3"));
    }

    @Test
    public void div_zero() {
        Assert.assertFalse(new CoreCalculator().calculate("1/0"));
    }
}